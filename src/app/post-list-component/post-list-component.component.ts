import { Component, Input,  OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

	@Input() title: string;
	@Input() content: string;
	@Input() loveIts: number;
	laDate= new Date;

  constructor() { }

  ngOnInit() {
  }

  getColor()
  {
  	if (this.loveIts > 0)
  	{
  		return 'green';
  	}
  	else if (this.loveIts < 0)
  	{
  		return 'red';
  	}
  }

  onLove(bouton)
  {
  	if (bouton == "love") 
  	{
  		this.loveIts ++;
  	}
  	else if (bouton == "nolove") 
  	{
  		this.loveIts --;
  	}
  }
}
