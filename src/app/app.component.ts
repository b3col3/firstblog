import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'FirstBlog';

  posts = [
  {
  	title: 'Mon premier post',
  	content: 'blabla',
  	loveIts: 0,
  },
  {
  	title: 'Mon deuxième post',
  	content: 'blabla',
  	loveIts: 0,
  },
  {
  	title: 'Encore un post',
  	content: 'blabla',
  	loveIts: 0,
  }
  ];
}
